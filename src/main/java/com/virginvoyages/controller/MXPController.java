package com.virginvoyages.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.virginvoyages.model.MXP;
import com.virginvoyages.model.mxp.MXPMockMessage;
import com.virginvoyages.model.mxp.Pl;
import com.virginvoyages.model.mxp.TeamMemberRole;
import com.virginvoyages.service.MXPService;
import com.virginvoyages.service.Producer;

@RestController
public class MXPController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MXPController.class);

	@Autowired
	private MXPService service;

	@Autowired
	private Producer producer;

	@GetMapping(path = "/mxpValidate/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getMxpID(@PathVariable String username) {
		LOGGER.info("MXP Input " + username);
		Optional<MXP> mxp = service.getMxpID(username);
		if (mxp.isPresent()) {
			LOGGER.info("MXP User Exists");
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(path = "/mxpUpdate/{username}")
	public ResponseEntity<String> updateEmployee(@PathVariable String username, @RequestBody MXP mxpRequest) {
		Optional<MXP> mxp = service.getMxpID(username);
		if (mxp.isPresent()) {
			int size = service.updateMxpID(username, mxpRequest);

			LOGGER.info("Update MXP" + size);
			if (size > 0) {
				MXPMockMessage mxpMockMessage = new MXPMockMessage();
				Pl pl = new Pl();
				TeamMemberRole teamMemberRole = new TeamMemberRole();
				List<TeamMemberRole> teamMemberRoleList = new ArrayList();

				pl.setTeamMemberUserName(mxpRequest.getUsername());
				pl.setFirstName(mxp.get().getFirst_name());
				pl.setMiddleName(mxp.get().getMiddle_name());
				pl.setLastName(mxp.get().getMiddle_name());
				pl.setDepartmentKey("Culinary");
				pl.setEmail(mxp.get().getPerson_primary_email());

				teamMemberRole.setRoleCode("100000639");
				teamMemberRole.setTeamMemberRoleName("Cook");
				teamMemberRoleList.add(teamMemberRole);
				pl.setTeamMemberRoles(teamMemberRoleList);

				mxpMockMessage.setPl(pl);

				producer.sendMessage(mxpMockMessage); // Kafka Queue
				return new ResponseEntity<>(HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}
		} else {
			int size = service.createMxpID(mxpRequest);
			if (size > 0) {
				MXPMockMessage mxpMockMessage = new MXPMockMessage();
				Pl pl = new Pl();
				TeamMemberRole teamMemberRole = new TeamMemberRole();
				List<TeamMemberRole> teamMemberRoleList = new ArrayList();

				pl.setTeamMemberUserName(mxpRequest.getUsername());
				pl.setFirstName(mxpRequest.getFirst_name());
				pl.setMiddleName(mxpRequest.getMiddle_name());
				pl.setLastName(mxpRequest.getMiddle_name());
				pl.setDepartmentKey("Culinary");
				pl.setEmail(mxpRequest.getPerson_primary_email());

				teamMemberRole.setRoleCode("100000639");
				teamMemberRole.setTeamMemberRoleName("Cook");
				teamMemberRoleList.add(teamMemberRole);
				pl.setTeamMemberRoles(teamMemberRoleList);

				mxpMockMessage.setPl(pl);

				producer.sendMessage(mxpMockMessage); // Kafka Queue
				return new ResponseEntity<>(HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}
		}
	}
}
