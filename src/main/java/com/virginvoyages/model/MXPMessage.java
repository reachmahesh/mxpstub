package com.virginvoyages.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MXPMessage implements Serializable {

    private static final long serialVersionUID = 5177728061271936143L;

    private String teamMemberUserName;

    private String email;

    private String firstName;

    private String middleName;

    private String lastName;

    private String departmentKey;

    private final List<TeamMemberRole> teamMemberRoles = new ArrayList<>();

    public String getTeamMemberUserName() {
        return teamMemberUserName;
    }

    public void setTeamMemberUserName(final String teamMemberUserName) {
        this.teamMemberUserName = teamMemberUserName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(final String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getDepartmentKey() {
        return departmentKey;
    }

    public void setDepartmentKey(final String departmentKey) {
        this.departmentKey = departmentKey;
    }

    public List<TeamMemberRole> getTeamMemberRoles() {
        return teamMemberRoles;
    }

    @Override
    public String toString() {
        return "MXPMessage{"
                + "teamMemberUserName=" + teamMemberUserName
                + ", email=" + email
                + ", firstName=" + firstName
                + ", middleName=" + middleName
                + ", lastName=" + lastName
                + ", departmentKey=" + departmentKey
                + ", teamMemberRoles=" + teamMemberRoles
                + '}';
    }
}
