package com.virginvoyages.model;

import java.io.Serializable;

public class TeamMemberRole implements Serializable {

    private static final long serialVersionUID = 9059586785278478723L;

    private String teamMemberRoleName;

    private String roleCode;

    public String getTeamMemberRoleName() {
        return teamMemberRoleName;
    }

    public void setTeamMemberRoleName(final String teamMemberRoleName) {
        this.teamMemberRoleName = teamMemberRoleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(final String roleCode) {
        this.roleCode = roleCode;
    }

    @Override
    public String toString() {
        return "TeamMemberRole{"
                + "teamMemberRoleName=" + teamMemberRoleName
                + ", roleCode=" + roleCode
                + '}';
    }
}