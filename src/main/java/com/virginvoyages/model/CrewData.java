package com.virginvoyages.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class CrewData {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({ "charge_id", "first_name", "middle_name", "last_name", "gender", "title", "place_of_birth",
			"date_of_birth", "country_of_birth", "country_of_residence", "country_of_nationality", "marital_status_id",
			"is_person_active", "installation_code", "room_nr", "arrival_date", "departure_date", "is_checked_in",
			"person_id", "PIN", "mxp_user", "username", "position_name", "department_name", "passport_type",
			"passport_number", "passport_first_name", "passport_last_name", "passport_issued_by",
			"passport_issued_country", "passport_issued_city", "passport_issued_date", "passport_expiration_date",
			"passport_is_primary", "person_primary_email", "embarkation_city_code", "debarkation_city_code",
			"manifest_type" })

	@JsonProperty("charge_id")
	private int chargeId;
	@JsonProperty("first_name")
	private String firstName;
	@JsonProperty("middle_name")
	private String middleName;
	@JsonProperty("last_name")
	private String lastName;
	@JsonProperty("gender")
	private String gender;
	@JsonProperty("title")
	private String title;
	@JsonProperty("place_of_birth")
	private String placeOfBirth;
	@JsonProperty("date_of_birth")
	private String dateOfBirth;
	@JsonProperty("country_of_birth")
	private Object countryOfBirth;
	@JsonProperty("country_of_residence")
	private Object countryOfResidence;
	@JsonProperty("country_of_nationality")
	private Object countryOfNationality;
	@JsonProperty("marital_status_id")
	private int maritalStatusId;
	@JsonProperty("is_person_active")
	private boolean isPersonActive;
	@JsonProperty("installation_code")
	private String installationCode;
	@JsonProperty("room_nr")
	private String roomNr;
	@JsonProperty("arrival_date")
	private String arrivalDate;
	@JsonProperty("departure_date")
	private String departureDate;
	@JsonProperty("is_checked_in")
	private boolean isCheckedIn;
	@JsonProperty("person_id")
	private int personId;
	@JsonProperty("PIN")
	private int pIN;
	@JsonProperty("mxp_user")
	private boolean mxpUser;
	@JsonProperty("username")
	private String username;
	@JsonProperty("position_name")
	private String positionName;
	@JsonProperty("department_name")
	private String departmentName;
	@JsonProperty("passport_type")
	private int passportType;
	@JsonProperty("passport_number")
	private String passportNumber;
	@JsonProperty("passport_first_name")
	private String passportFirstName;
	@JsonProperty("passport_last_name")
	private String passportLastName;
	@JsonProperty("passport_issued_by")
	private String passportIssuedBy;
	@JsonProperty("passport_issued_country")
	private String passportIssuedCountry;
	@JsonProperty("passport_issued_city")
	private String passportIssuedCity;
	@JsonProperty("passport_issued_date")
	private String passportIssuedDate;
	@JsonProperty("passport_expiration_date")
	private String passportExpirationDate;
	@JsonProperty("passport_is_primary")
	private boolean passportIsPrimary;
	@JsonProperty("person_primary_email")
	private String personPrimaryEmail;
	@JsonProperty("embarkation_city_code")
	private String embarkationCityCode;
	@JsonProperty("debarkation_city_code")
	private String debarkationCityCode;
	@JsonProperty("manifest_type")
	private String manifestType;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("charge_id")
	public int getChargeId() {
		return chargeId;
	}

	@JsonProperty("charge_id")
	public void setChargeId(int chargeId) {
		this.chargeId = chargeId;
	}

	@JsonProperty("first_name")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty("first_name")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("middle_name")
	public String getMiddleName() {
		return middleName;
	}

	@JsonProperty("middle_name")
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@JsonProperty("last_name")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("last_name")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("place_of_birth")
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	@JsonProperty("place_of_birth")
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	@JsonProperty("date_of_birth")
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	@JsonProperty("date_of_birth")
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@JsonProperty("country_of_birth")
	public Object getCountryOfBirth() {
		return countryOfBirth;
	}

	@JsonProperty("country_of_birth")
	public void setCountryOfBirth(Object countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	@JsonProperty("country_of_residence")
	public Object getCountryOfResidence() {
		return countryOfResidence;
	}

	@JsonProperty("country_of_residence")
	public void setCountryOfResidence(Object countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	@JsonProperty("country_of_nationality")
	public Object getCountryOfNationality() {
		return countryOfNationality;
	}

	@JsonProperty("country_of_nationality")
	public void setCountryOfNationality(Object countryOfNationality) {
		this.countryOfNationality = countryOfNationality;
	}

	@JsonProperty("marital_status_id")
	public int getMaritalStatusId() {
		return maritalStatusId;
	}

	@JsonProperty("marital_status_id")
	public void setMaritalStatusId(int maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	@JsonProperty("is_person_active")
	public boolean isIsPersonActive() {
		return isPersonActive;
	}

	@JsonProperty("is_person_active")
	public void setIsPersonActive(boolean isPersonActive) {
		this.isPersonActive = isPersonActive;
	}

	@JsonProperty("installation_code")
	public String getInstallationCode() {
		return installationCode;
	}

	@JsonProperty("installation_code")
	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}

	@JsonProperty("room_nr")
	public String getRoomNr() {
		return roomNr;
	}

	@JsonProperty("room_nr")
	public void setRoomNr(String roomNr) {
		this.roomNr = roomNr;
	}

	@JsonProperty("arrival_date")
	public String getArrivalDate() {
		return arrivalDate;
	}

	@JsonProperty("arrival_date")
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@JsonProperty("departure_date")
	public String getDepartureDate() {
		return departureDate;
	}

	@JsonProperty("departure_date")
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	@JsonProperty("is_checked_in")
	public boolean isIsCheckedIn() {
		return isCheckedIn;
	}

	@JsonProperty("is_checked_in")
	public void setIsCheckedIn(boolean isCheckedIn) {
		this.isCheckedIn = isCheckedIn;
	}

	@JsonProperty("person_id")
	public int getPersonId() {
		return personId;
	}

	@JsonProperty("person_id")
	public void setPersonId(int personId) {
		this.personId = personId;
	}

	@JsonProperty("PIN")
	public int getPIN() {
		return pIN;
	}

	@JsonProperty("PIN")
	public void setPIN(int pIN) {
		this.pIN = pIN;
	}

	@JsonProperty("mxp_user")
	public boolean isMxpUser() {
		return mxpUser;
	}

	@JsonProperty("mxp_user")
	public void setMxpUser(boolean mxpUser) {
		this.mxpUser = mxpUser;
	}

	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("position_name")
	public String getPositionName() {
		return positionName;
	}

	@JsonProperty("position_name")
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	@JsonProperty("department_name")
	public String getDepartmentName() {
		return departmentName;
	}

	@JsonProperty("department_name")
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@JsonProperty("passport_type")
	public int getPassportType() {
		return passportType;
	}

	@JsonProperty("passport_type")
	public void setPassportType(int passportType) {
		this.passportType = passportType;
	}

	@JsonProperty("passport_number")
	public String getPassportNumber() {
		return passportNumber;
	}

	@JsonProperty("passport_number")
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	@JsonProperty("passport_first_name")
	public String getPassportFirstName() {
		return passportFirstName;
	}

	@JsonProperty("passport_first_name")
	public void setPassportFirstName(String passportFirstName) {
		this.passportFirstName = passportFirstName;
	}

	@JsonProperty("passport_last_name")
	public String getPassportLastName() {
		return passportLastName;
	}

	@JsonProperty("passport_last_name")
	public void setPassportLastName(String passportLastName) {
		this.passportLastName = passportLastName;
	}

	@JsonProperty("passport_issued_by")
	public String getPassportIssuedBy() {
		return passportIssuedBy;
	}

	@JsonProperty("passport_issued_by")
	public void setPassportIssuedBy(String passportIssuedBy) {
		this.passportIssuedBy = passportIssuedBy;
	}

	@JsonProperty("passport_issued_country")
	public String getPassportIssuedCountry() {
		return passportIssuedCountry;
	}

	@JsonProperty("passport_issued_country")
	public void setPassportIssuedCountry(String passportIssuedCountry) {
		this.passportIssuedCountry = passportIssuedCountry;
	}

	@JsonProperty("passport_issued_city")
	public String getPassportIssuedCity() {
		return passportIssuedCity;
	}

	@JsonProperty("passport_issued_city")
	public void setPassportIssuedCity(String passportIssuedCity) {
		this.passportIssuedCity = passportIssuedCity;
	}

	@JsonProperty("passport_issued_date")
	public String getPassportIssuedDate() {
		return passportIssuedDate;
	}

	@JsonProperty("passport_issued_date")
	public void setPassportIssuedDate(String passportIssuedDate) {
		this.passportIssuedDate = passportIssuedDate;
	}

	@JsonProperty("passport_expiration_date")
	public String getPassportExpirationDate() {
		return passportExpirationDate;
	}

	@JsonProperty("passport_expiration_date")
	public void setPassportExpirationDate(String passportExpirationDate) {
		this.passportExpirationDate = passportExpirationDate;
	}

	@JsonProperty("passport_is_primary")
	public boolean isPassportIsPrimary() {
		return passportIsPrimary;
	}

	@JsonProperty("passport_is_primary")
	public void setPassportIsPrimary(boolean passportIsPrimary) {
		this.passportIsPrimary = passportIsPrimary;
	}

	@JsonProperty("person_primary_email")
	public String getPersonPrimaryEmail() {
		return personPrimaryEmail;
	}

	@JsonProperty("person_primary_email")
	public void setPersonPrimaryEmail(String personPrimaryEmail) {
		this.personPrimaryEmail = personPrimaryEmail;
	}

	@JsonProperty("embarkation_city_code")
	public String getEmbarkationCityCode() {
		return embarkationCityCode;
	}

	@JsonProperty("embarkation_city_code")
	public void setEmbarkationCityCode(String embarkationCityCode) {
		this.embarkationCityCode = embarkationCityCode;
	}

	@JsonProperty("debarkation_city_code")
	public String getDebarkationCityCode() {
		return debarkationCityCode;
	}

	@JsonProperty("debarkation_city_code")
	public void setDebarkationCityCode(String debarkationCityCode) {
		this.debarkationCityCode = debarkationCityCode;
	}

	@JsonProperty("manifest_type")
	public String getManifestType() {
		return manifestType;
	}

	@JsonProperty("manifest_type")
	public void setManifestType(String manifestType) {
		this.manifestType = manifestType;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}