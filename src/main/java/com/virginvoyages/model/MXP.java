package com.virginvoyages.model;

public class MXP {

	private String username;
	private String first_name;
	private String last_name;
	private String middle_name;
	private String person_primary_email;

	public MXP() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MXP(String username, String first_name, String last_name, String middle_name, String person_primary_email) {
		super();
		this.username = username;
		this.first_name = first_name;
		this.last_name = last_name;
		this.middle_name = middle_name;
		this.person_primary_email = person_primary_email;
	}

	@Override
	public String toString() {
		return "MXP [username=" + username + ", first_name=" + first_name + ", last_name=" + last_name
				+ ", middle_name=" + middle_name + ", person_primary_email=" + person_primary_email + "]";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getPerson_primary_email() {
		return person_primary_email;
	}

	public void setPerson_primary_email(String person_primary_email) {
		this.person_primary_email = person_primary_email;
	}

}
