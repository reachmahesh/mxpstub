package com.virginvoyages.model.mxp;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ci", "source", "ts", "n", "cn", "tg", "plt", "mrc", "rc", "lex", "t", "ri", "k", "pl", "status" })
public class MXPMockMessage {

	@JsonProperty("ci")
	private String ci;
	@JsonProperty("source")
	private Object source;
	@JsonProperty("ts")
	private String ts;
	@JsonProperty("n")
	private String n;
	@JsonProperty("cn")
	private String cn;
	@JsonProperty("tg")
	private String tg;
	@JsonProperty("plt")
	private String plt;
	@JsonProperty("mrc")
	private int mrc;
	@JsonProperty("rc")
	private int rc;
	@JsonProperty("lex")
	private Object lex;
	@JsonProperty("t")
	private Object t;
	@JsonProperty("ri")
	private String ri;
	@JsonProperty("k")
	private Object k;
	@JsonProperty("pl")
	private Pl pl;
	@JsonProperty("status")
	private String status;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("ci")
	public String getCi() {
		return ci;
	}

	@JsonProperty("ci")
	public void setCi(String ci) {
		this.ci = ci;
	}

	@JsonProperty("source")
	public Object getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(Object source) {
		this.source = source;
	}

	@JsonProperty("ts")
	public String getTs() {
		return ts;
	}

	@JsonProperty("ts")
	public void setTs(String ts) {
		this.ts = ts;
	}

	@JsonProperty("n")
	public String getN() {
		return n;
	}

	@JsonProperty("n")
	public void setN(String n) {
		this.n = n;
	}

	@JsonProperty("cn")
	public String getCn() {
		return cn;
	}

	@JsonProperty("cn")
	public void setCn(String cn) {
		this.cn = cn;
	}

	@JsonProperty("tg")
	public String getTg() {
		return tg;
	}

	@JsonProperty("tg")
	public void setTg(String tg) {
		this.tg = tg;
	}

	@JsonProperty("plt")
	public String getPlt() {
		return plt;
	}

	@JsonProperty("plt")
	public void setPlt(String plt) {
		this.plt = plt;
	}

	@JsonProperty("mrc")
	public int getMrc() {
		return mrc;
	}

	@JsonProperty("mrc")
	public void setMrc(int mrc) {
		this.mrc = mrc;
	}

	@JsonProperty("rc")
	public int getRc() {
		return rc;
	}

	@JsonProperty("rc")
	public void setRc(int rc) {
		this.rc = rc;
	}

	@JsonProperty("lex")
	public Object getLex() {
		return lex;
	}

	@JsonProperty("lex")
	public void setLex(Object lex) {
		this.lex = lex;
	}

	@JsonProperty("t")
	public Object getT() {
		return t;
	}

	@JsonProperty("t")
	public void setT(Object t) {
		this.t = t;
	}

	@JsonProperty("ri")
	public String getRi() {
		return ri;
	}

	@JsonProperty("ri")
	public void setRi(String ri) {
		this.ri = ri;
	}

	@JsonProperty("k")
	public Object getK() {
		return k;
	}

	@JsonProperty("k")
	public void setK(Object k) {
		this.k = k;
	}

	@JsonProperty("pl")
	public Pl getPl() {
		return pl;
	}

	@JsonProperty("pl")
	public void setPl(Pl pl) {
		this.pl = pl;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
