package com.virginvoyages.model.mxp;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"teamMemberRoleName",
"roleCode"
})
public class TeamMemberRole {

@JsonProperty("teamMemberRoleName")
private String teamMemberRoleName;
@JsonProperty("roleCode")
private String roleCode;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("teamMemberRoleName")
public String getTeamMemberRoleName() {
return teamMemberRoleName;
}

@JsonProperty("teamMemberRoleName")
public void setTeamMemberRoleName(String teamMemberRoleName) {
this.teamMemberRoleName = teamMemberRoleName;
}

@JsonProperty("roleCode")
public String getRoleCode() {
return roleCode;
}

@JsonProperty("roleCode")
public void setRoleCode(String roleCode) {
this.roleCode = roleCode;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}