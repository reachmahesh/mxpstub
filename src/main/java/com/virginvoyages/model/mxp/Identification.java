package com.virginvoyages.model.mxp;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"documentTypeCode",
"number",
"issueCountryCode",
"expiryDate"
})
public class Identification {

@JsonProperty("documentTypeCode")
private String documentTypeCode;
@JsonProperty("number")
private String number;
@JsonProperty("issueCountryCode")
private String issueCountryCode;
@JsonProperty("expiryDate")
private String expiryDate;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("documentTypeCode")
public String getDocumentTypeCode() {
return documentTypeCode;
}

@JsonProperty("documentTypeCode")
public void setDocumentTypeCode(String documentTypeCode) {
this.documentTypeCode = documentTypeCode;
}

@JsonProperty("number")
public String getNumber() {
return number;
}

@JsonProperty("number")
public void setNumber(String number) {
this.number = number;
}

@JsonProperty("issueCountryCode")
public String getIssueCountryCode() {
return issueCountryCode;
}

@JsonProperty("issueCountryCode")
public void setIssueCountryCode(String issueCountryCode) {
this.issueCountryCode = issueCountryCode;
}

@JsonProperty("expiryDate")
public String getExpiryDate() {
return expiryDate;
}

@JsonProperty("expiryDate")
public void setExpiryDate(String expiryDate) {
this.expiryDate = expiryDate;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}
}
