package com.virginvoyages.model.mxp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"teamMemberNumber",
"RFID",
"positionServiceLocation",
"departmentKey",
"safetyNumber",
"chargeID",
"titleCode",
"firstName",
"middleName",
"lastName",
"stateroom",
"birthDate",
"citizenshipCountrycode",
"gender",
"email",
"planned",
"joiningDate",
"embarkDate",
"actualDebarkDate",
"debarkDate",
"crewPhoto",
"photoModifiedDate",
"teamMemberUserName",
"teamMemberDuties",
"NotcheckedIn",
"teamMemberRoles",
"phones",
"visas",
"identifications",
"addresses"
})
public class Pl {

@JsonProperty("teamMemberNumber")
private String teamMemberNumber;
@JsonProperty("RFID")
private String rFID;
@JsonProperty("positionServiceLocation")
private String positionServiceLocation;
@JsonProperty("departmentKey")
private String departmentKey;
@JsonProperty("safetyNumber")
private String safetyNumber;
@JsonProperty("chargeID")
private String chargeID;
@JsonProperty("titleCode")
private String titleCode;
@JsonProperty("firstName")
private String firstName;
@JsonProperty("middleName")
private String middleName;
@JsonProperty("lastName")
private String lastName;
@JsonProperty("stateroom")
private String stateroom;
@JsonProperty("birthDate")
private String birthDate;
@JsonProperty("citizenshipCountrycode")
private String citizenshipCountrycode;
@JsonProperty("gender")
private String gender;
@JsonProperty("email")
private String email;
@JsonProperty("planned")
private String planned;
@JsonProperty("joiningDate")
private String joiningDate;
@JsonProperty("embarkDate")
private String embarkDate;
@JsonProperty("actualDebarkDate")
private String actualDebarkDate;
@JsonProperty("debarkDate")
private String debarkDate;
@JsonProperty("crewPhoto")
private String crewPhoto;
@JsonProperty("photoModifiedDate")
private String photoModifiedDate;
@JsonProperty("teamMemberUserName")
private String teamMemberUserName;
@JsonProperty("teamMemberDuties")
private List<Object> teamMemberDuties = null;
@JsonProperty("NotcheckedIn")
private String notcheckedIn;
@JsonProperty("teamMemberRoles")
private List<TeamMemberRole> teamMemberRoles = null;
@JsonProperty("phones")
private List<Phone> phones = null;
@JsonProperty("visas")
private List<Object> visas = null;
@JsonProperty("identifications")
private List<Identification> identifications = null;
@JsonProperty("addresses")
private List<Address> addresses = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("teamMemberNumber")
public String getTeamMemberNumber() {
return teamMemberNumber;
}

@JsonProperty("teamMemberNumber")
public void setTeamMemberNumber(String teamMemberNumber) {
this.teamMemberNumber = teamMemberNumber;
}

@JsonProperty("RFID")
public String getRFID() {
return rFID;
}

@JsonProperty("RFID")
public void setRFID(String rFID) {
this.rFID = rFID;
}

@JsonProperty("positionServiceLocation")
public String getPositionServiceLocation() {
return positionServiceLocation;
}

@JsonProperty("positionServiceLocation")
public void setPositionServiceLocation(String positionServiceLocation) {
this.positionServiceLocation = positionServiceLocation;
}

@JsonProperty("departmentKey")
public String getDepartmentKey() {
return departmentKey;
}

@JsonProperty("departmentKey")
public void setDepartmentKey(String departmentKey) {
this.departmentKey = departmentKey;
}

@JsonProperty("safetyNumber")
public String getSafetyNumber() {
return safetyNumber;
}

@JsonProperty("safetyNumber")
public void setSafetyNumber(String safetyNumber) {
this.safetyNumber = safetyNumber;
}

@JsonProperty("chargeID")
public String getChargeID() {
return chargeID;
}

@JsonProperty("chargeID")
public void setChargeID(String chargeID) {
this.chargeID = chargeID;
}

@JsonProperty("titleCode")
public String getTitleCode() {
return titleCode;
}

@JsonProperty("titleCode")
public void setTitleCode(String titleCode) {
this.titleCode = titleCode;
}

@JsonProperty("firstName")
public String getFirstName() {
return firstName;
}

@JsonProperty("firstName")
public void setFirstName(String firstName) {
this.firstName = firstName;
}

@JsonProperty("middleName")
public String getMiddleName() {
return middleName;
}

@JsonProperty("middleName")
public void setMiddleName(String middleName) {
this.middleName = middleName;
}

@JsonProperty("lastName")
public String getLastName() {
return lastName;
}

@JsonProperty("lastName")
public void setLastName(String lastName) {
this.lastName = lastName;
}

@JsonProperty("stateroom")
public String getStateroom() {
return stateroom;
}

@JsonProperty("stateroom")
public void setStateroom(String stateroom) {
this.stateroom = stateroom;
}

@JsonProperty("birthDate")
public String getBirthDate() {
return birthDate;
}

@JsonProperty("birthDate")
public void setBirthDate(String birthDate) {
this.birthDate = birthDate;
}

@JsonProperty("citizenshipCountrycode")
public String getCitizenshipCountrycode() {
return citizenshipCountrycode;
}

@JsonProperty("citizenshipCountrycode")
public void setCitizenshipCountrycode(String citizenshipCountrycode) {
this.citizenshipCountrycode = citizenshipCountrycode;
}

@JsonProperty("gender")
public String getGender() {
return gender;
}

@JsonProperty("gender")
public void setGender(String gender) {
this.gender = gender;
}

@JsonProperty("email")
public String getEmail() {
return email;
}

@JsonProperty("email")
public void setEmail(String email) {
this.email = email;
}

@JsonProperty("planned")
public String getPlanned() {
return planned;
}

@JsonProperty("planned")
public void setPlanned(String planned) {
this.planned = planned;
}

@JsonProperty("joiningDate")
public String getJoiningDate() {
return joiningDate;
}

@JsonProperty("joiningDate")
public void setJoiningDate(String joiningDate) {
this.joiningDate = joiningDate;
}

@JsonProperty("embarkDate")
public String getEmbarkDate() {
return embarkDate;
}

@JsonProperty("embarkDate")
public void setEmbarkDate(String embarkDate) {
this.embarkDate = embarkDate;
}

@JsonProperty("actualDebarkDate")
public String getActualDebarkDate() {
return actualDebarkDate;
}

@JsonProperty("actualDebarkDate")
public void setActualDebarkDate(String actualDebarkDate) {
this.actualDebarkDate = actualDebarkDate;
}

@JsonProperty("debarkDate")
public String getDebarkDate() {
return debarkDate;
}

@JsonProperty("debarkDate")
public void setDebarkDate(String debarkDate) {
this.debarkDate = debarkDate;
}

@JsonProperty("crewPhoto")
public String getCrewPhoto() {
return crewPhoto;
}

@JsonProperty("crewPhoto")
public void setCrewPhoto(String crewPhoto) {
this.crewPhoto = crewPhoto;
}

@JsonProperty("photoModifiedDate")
public String getPhotoModifiedDate() {
return photoModifiedDate;
}

@JsonProperty("photoModifiedDate")
public void setPhotoModifiedDate(String photoModifiedDate) {
this.photoModifiedDate = photoModifiedDate;
}

@JsonProperty("teamMemberUserName")
public String getTeamMemberUserName() {
return teamMemberUserName;
}

@JsonProperty("teamMemberUserName")
public void setTeamMemberUserName(String teamMemberUserName) {
this.teamMemberUserName = teamMemberUserName;
}

@JsonProperty("teamMemberDuties")
public List<Object> getTeamMemberDuties() {
return teamMemberDuties;
}

@JsonProperty("teamMemberDuties")
public void setTeamMemberDuties(List<Object> teamMemberDuties) {
this.teamMemberDuties = teamMemberDuties;
}

@JsonProperty("NotcheckedIn")
public String getNotcheckedIn() {
return notcheckedIn;
}

@JsonProperty("NotcheckedIn")
public void setNotcheckedIn(String notcheckedIn) {
this.notcheckedIn = notcheckedIn;
}

@JsonProperty("teamMemberRoles")
public List<TeamMemberRole> getTeamMemberRoles() {
return teamMemberRoles;
}

@JsonProperty("teamMemberRoles")
public void setTeamMemberRoles(List<TeamMemberRole> teamMemberRoles) {
this.teamMemberRoles = teamMemberRoles;
}

@JsonProperty("phones")
public List<Phone> getPhones() {
return phones;
}

@JsonProperty("phones")
public void setPhones(List<Phone> phones) {
this.phones = phones;
}

@JsonProperty("visas")
public List<Object> getVisas() {
return visas;
}

@JsonProperty("visas")
public void setVisas(List<Object> visas) {
this.visas = visas;
}

@JsonProperty("identifications")
public List<Identification> getIdentifications() {
return identifications;
}

@JsonProperty("identifications")
public void setIdentifications(List<Identification> identifications) {
this.identifications = identifications;
}

@JsonProperty("addresses")
public List<Address> getAddresses() {
return addresses;
}

@JsonProperty("addresses")
public void setAddresses(List<Address> addresses) {
this.addresses = addresses;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
