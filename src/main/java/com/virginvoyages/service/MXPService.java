package com.virginvoyages.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virginvoyages.dao.MXPDao;
import com.virginvoyages.model.MXP;

@Service
public class MXPService {

	@Autowired
	private MXPDao mxpRepository;

	public Optional<MXP> getMxpID(String id) {
		return mxpRepository.findMxpById(id);
	}

	public int updateMxpID(String oldUsername, MXP mxpRequest) {
		return mxpRepository.saveMXP(oldUsername, mxpRequest);
	}

	public int createMxpID(MXP mxpRequest) {
		return mxpRepository.createMXP(mxpRequest);
	}
}
