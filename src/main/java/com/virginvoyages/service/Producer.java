package com.virginvoyages.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.virginvoyages.model.mxp.MXPMockMessage;

@Service
public class Producer {
	private static final Logger logger = LoggerFactory.getLogger(Producer.class);
	@Autowired
	private KafkaTemplate<String, MXPMockMessage> kafkaTemplate;

	@Value("${app.topic.mxpstub}")
    private String topic;
	
	public void sendMessage(MXPMockMessage data) {
		logger.info(String.format("$$ -> Producing message --> %s", data));
		// this.kafkaTemplate.send(AppConstants.TOPIC_NAME, data);

		this.kafkaTemplate.send(topic, data);

	}
}