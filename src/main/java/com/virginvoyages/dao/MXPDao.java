package com.virginvoyages.dao;

import java.util.Optional;

import com.virginvoyages.model.MXP;

public interface MXPDao {

	Optional<MXP> findMxpById(String id);

	int saveMXP(String oldUsername, MXP mxpRequest);

	int createMXP(MXP mxpRequest);
}