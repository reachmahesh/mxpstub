package com.virginvoyages.dao.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.virginvoyages.dao.MXPDao;
import com.virginvoyages.model.MXP;

@Repository
public class MXPDaoImpl implements MXPDao {

	private static final Logger log = LoggerFactory.getLogger(MXPDaoImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Optional<MXP> findMxpById(String username) {
		log.info("findMxpByUSER Query" + "select * from mxpuser where upper(username)="
				+ username.toUpperCase().trim());
            MXP result = null;
            try {
                result = jdbcTemplate.queryForObject(
                        "select * from mxpuser where upper(username) = ?",
                        new Object[] { username.toUpperCase().trim() },
                        (rs, rowNum) -> new MXP(
                                rs.getString("username"), 
                                rs.getString("first_name"), 
                                rs.getString("last_name"),
                                rs.getString("middle_name"),
                                rs.getString("person_primary_email")));
            } catch (IncorrectResultSizeDataAccessException e) {
                log.debug("Expected to find exactly one MXP given username = {}", username, e);
            }

            return Optional.ofNullable(result);
	}

	@Override
	public int saveMXP(String oldUsername, MXP mxpRequest) {
		int size = 0;
		if (!findMxpById(oldUsername).isPresent()) {
			size = jdbcTemplate.update(
					"insert into mxpuser (username, first_name, last_name, middle_name,person_primary_email) values(?,?,?,?,?)",
					oldUsername.trim(), mxpRequest.getFirst_name(), mxpRequest.getMiddle_name(),mxpRequest.getLast_name(),mxpRequest.getPerson_primary_email());
		
		}else {
			size = jdbcTemplate.update(
					"update mxpuser set username=? where username=?",
					mxpRequest.getUsername().trim(), oldUsername.trim());
			
		}
		return size;
	}
	
	@Override
	public int createMXP(MXP mxpRequest) {
		int size = 0;
		if (!findMxpById(mxpRequest.getUsername()).isPresent()) {
			size = jdbcTemplate.update(
					"insert into mxpuser (username, first_name, last_name, middle_name,person_primary_email) values(?,?,?,?,?)",
					mxpRequest.getUsername().trim(), mxpRequest.getFirst_name(), mxpRequest.getMiddle_name(),mxpRequest.getLast_name(),mxpRequest.getPerson_primary_email());
		
		}
		return size;
	}

}
