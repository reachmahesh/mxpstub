CREATE DATABASE IF NOT EXISTS mxp;
USE mxp;

drop table mxpuser 
CREATE TABLE mxpuser (username VARCHAR(100) NOT NULL, first_name VARCHAR(150) NOT NULL, last_name VARCHAR(150), middle_name VARCHAR(150), person_primary_email varchar(150), PRIMARY KEY (username));
 
INSERT INTO mxpuser VALUES ('JJOHN', 'Jack', 'Johnson', 'Charlie', 'john.j@gmail.com');
INSERT INTO mxpuser VALUES ('JJOHN1', 'Jack1', 'Johnson', 'Charlie', 'john.j1@gmail.com');
INSERT INTO mxpuser VALUES ('JJOHN2', 'Jack2', 'Johnson', 'Charlie', 'john.j2@gmail.com');
INSERT INTO mxpuser VALUES ('JJOHN3', 'Jack3', 'Johnson', 'Charlie', 'john.j3@gmail.com');
INSERT INTO mxpuser VALUES ('JJOHN4', 'Jack4', 'Johnson', 'Charlie', 'john.j4@gmail.com');
